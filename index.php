<!DOCTYPE html>
<head>
  <title>Learning project</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


<?php  require_once 'process.php'; ?>
</head>

<body>

	<?php 
	 if (isset($_SESSION['message'])): ?>

	 	<div class="alert alert-<?=$_SESSION['msg_type']?>">

	 	<?php 
	 	echo $_SESSION['message'];
	 	unset($_SESSION['message']); 
	 	?> 
	 	</div>	
	 	<?php endif; ?>


	
	<?php 

	$mysqli = new mysqli('localhost', 'root', '', 'my') or die (mysqli_error($mysqli));
	$sqli = "SELECT * FROM data";
	$result = $mysqli-> query($sqli); //or die ($mysqli->error);
		//pre_r ($result);
	 	?>
	 	<div class="">
	 		<table class="table table-hover">
	 		<thead>	
	 			<tr>
	 				<th>Name</th>
	 				<th>Location</th>
	 				<th colspan="2">• Action</th>
	 				
	 			</tr>
	 			</thead>

	 	<?php 
	 				while ($row = $result->fetch_assoc()):  ?>
	 			
	 			<tr>
	 				<td><?php echo $row['name'];     ?> </td>
	 				<td><?php echo $row['location']; ?> </td>
	 			<td>
	 				<a href="index.php?edit=<?php echo $row['id']; ?>"
	 					class="btn btn-info">Edit</a>
	 				<a href="process.php?delete=<?php echo $row['id']; ?> " onclick="return confirm ('Are you sure to delete?');"
	 					class="btn btn-danger">delete</a>
	 				</td>

	 			</tr>
	 			
	 				 <?php endwhile;  ?>

	 		</table>
	 	</div>

	 	<?php
		
		function pre_r($array) {
		echo '<pre>';
		print_r($array);
		echo '</pre>';

		}
	?>
	<div class=" row justify-content-center" width="100px">
		<form action="process.php" method="POST" >
			<input type="hidden" name="id" value="<?php echo $id ?>">
			<div class="form-group">
			<label>Name</label>
			<input type="text" name="name" class="form-control"
			value="<?php echo $name; ?>" placeholder="Enter your name">
			</div>
			<div class="form-group">
			<label>Location</label>
			<input type="text" name="location" class="form-control" 
			value="<?php echo $location; ?>" placeholder="Enter your location">
			</div>
			<div class="form-group">
				<?php 
				if ($update == true): ?> 
					<button type="submit" class="btn btn-primary"   name="update">Update </button>
				<?php else: ?>
					<button type="submit" class="btn btn-success"   name="save">Save </button>
				<?php	endif; ?>
		
			</div>
		</form>
	</div>

</body> 